import React from 'react';
import { Image } from 'react-native';
import logoW from '../../../assets/escorpiao.png'
import logoD from '../../../assets/inseto-folha.png'
import { Container, Title } from './styles';



export default function Header () {
  return (
    <Container>
      <Image
        source={logoW}
        style={{height:100,width: 100}}
        />
        <Title>Bichinhos!</Title>
    </Container>
  );
};