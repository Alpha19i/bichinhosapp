import React from 'react';
import Header from '../../components/Header';
import { ButtonSubmit, Container, Input, KeyboardView, TextButton} from './styles';

export default function SignIn() {
  return (
    <KeyboardView>
      
      <Header/>
     
      <Container>
        <Input
          placeholder="Nome"
          placeholderTextColor="#f0a"
          />
        <Input
          placeholder="Sobrenome"
          placeholderTextColor="#f0a"
        />
        <ButtonSubmit>
          <TextButton>Começar!</TextButton>
        </ButtonSubmit>
      </Container>  
    </KeyboardView>
  );
}