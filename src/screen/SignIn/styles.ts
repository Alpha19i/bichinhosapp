import styled from 'styled-components/native';

export const KeyboardView = styled.KeyboardAvoidingView`
      flex: 1;
      align-items: center;
      justify-content: center;
      background-color: #FFF;
    `
export const Container = styled.View`
      justify-content: center;
      align-items: center;
      padding-top: 30px;
      width: 90%;
      `

export const Input = styled.TextInput`
      border: 2px solid #f0a;
      margin-bottom: 30px;
      padding: 15px 60px;
      border-radius: 10px;
      width: 90%;
      font-size: 20px;
      color: #f0a;
      `

export const TextButton = styled.Text`   
      color: #FFF;
      font-size: 20px;
      font-weight: bold;
    `
export const ButtonSubmit = styled.TouchableOpacity`   
      align-items: center;
      background-color: #f0a;
      padding: 15px 60px;
      border-radius: 10px;
      width: 90%;
    `